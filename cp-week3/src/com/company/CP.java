package com.company;


import java.io.*;
import java.util.*;

public class CP {
    public List<Point> points = new ArrayList<Point>();

    public static void main(String[] params) throws Exception{
        String source_file_name = "data/closest-pair-out.txt";
        FileInputStream fis = new FileInputStream(source_file_name);
        BufferedReader rds = new BufferedReader(new InputStreamReader(fis));

        String line = null;
        while ((line = rds.readLine()) != null) {
            String[] pair = line.split(":");
            String realPath = pair[0].replace("../", "").replace(".tsp", "-tsp")+ ".txt";
            System.out.println("In: " + line);
            File file = new File(realPath);
            FileInputStream fstream = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            CP cp = new CP();
            cp.parse(br);
            Point[] xPoints = new Point[cp.points.size()];
            cp.points.toArray(xPoints);
            Point[] yPoints = new Point[cp.points.size()];
            cp.points.toArray(yPoints);
            Arrays.sort(xPoints, new Comparator<Point>() {
                @Override
                public int compare(Point o1, Point o2) {
                    return o1.x.compareTo(o2.x);
                }
            });
            Arrays.sort(yPoints, new Comparator<Point>() {
                @Override
                public int compare(Point o1, Point o2) {
                    return o1.y.compareTo(o2.y);
                }
            });
            Pair closestPair = cp.getClosestsPairs(xPoints, yPoints);
            System.out.println("Out: " + pair[0] + ": " + xPoints.length + " " + closestPair.getDistance());
        }
    }

    public CP(){}

    public Pair getClosestsPairs(Point[] xPoints, Point[] yPoints){
        if(xPoints.length <= 3){
            return recGetClosestsPairs(xPoints);
        }
        int half = xPoints.length / 2;
        Point halfPoint = xPoints[half];
        
        Point[] xRightHalf = Arrays.copyOfRange(xPoints, 0, half);
        Point[] xLeftHalf = Arrays.copyOfRange(xPoints, half, xPoints.length);
        Point[] yRightHalf = Arrays.copyOfRange(yPoints, 0, half);
        Point[] yLeftHalf = Arrays.copyOfRange(xPoints, half, yPoints.length);
        Pair left = getClosestsPairs(xLeftHalf, yLeftHalf);
        Pair right = getClosestsPairs(xRightHalf, yRightHalf);
        Pair deltaPoint = left.getDistance() < right.getDistance() ? left : right;

        List<Point> deltaPoints = new ArrayList<Point>();
        for(int i = 0; i < xPoints.length; i++){
            if(Math.abs(halfPoint.x - xPoints[i].x) < deltaPoint.getDistance()){
                deltaPoints.add(xPoints[i]);
            }
        }

        Pair minDist = deltaPoint;
        double minDistance = deltaPoint.getDistance();
        for(int i = 0; i < deltaPoints.size(); i++){
            int noOfNeighbors = i + 11 < deltaPoints.size() ? 10 : deltaPoints.size() - i;
            for(int j = 1; j < noOfNeighbors; j++){
                Pair dist = new Pair(deltaPoints.get(i), deltaPoints.get(i + j));
                if(dist.getDistance() < minDistance){
                    minDist = dist;
                    minDistance = dist.getDistance();
                }
            }
        }
        return minDist;
    }
    public Pair recGetClosestsPairs(Point[] points){

        double minDistance = Double.MAX_VALUE;
        Pair minDist = null;
        for(int i = 0; i < points.length; i++){
            for(int j = i + 1; j < points.length; j++){
                Pair dist = new Pair(points[i], points[j]);
                if(dist.getDistance() < minDistance){
                    minDist = dist;
                    minDistance = dist.getDistance();
                }
            }
        }
        return minDist;
    }

    public void parse(BufferedReader br)
    {
        String regex = "([\\s]*|[\\t]*)?([0-9a-zA-Z]+)([\\s]*|[\\t]*)([0-9\\+\\-e]+\\.?[0-9\\+\\-e]*)([\\s]*|[\\t]*)([0-9\\+\\-e]+\\.?([0-9\\+\\-e]*)?)";
        String strLine;
        try {
            int counter = 1;
            while ((strLine = br.readLine()) != null) {
                if(strLine.matches(regex)) {
                    String[] split = strLine.trim().split("\\s+");
                    String name = split[0];
                    double x = Double.parseDouble(split[1]);
                    double y = Double.parseDouble(split[2]);
                    Point p = new Point();
                    p.x = x;
                    p.y = y;
                    p.index = counter;
                    p.name = name;
                    points.add(p);
                    counter++;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public class Pair {
        public Point p1;
        public Point p2;

        public Pair(Point p1, Point p2){
            this.p1 = p1;
            this.p2 = p2;
        }
        public double getDistance(){
            return Math.sqrt(Math.pow(p2.x - p1.x, 2d) + Math.pow(p2.y - p1.y, 2));
        }

        @Override
        public String toString() {
            return p1.toString() + " " + p2.toString() + " dist:" + getDistance();
        }
    }
    public class Point {
        public int index;
        public String name;
        public Double x;
        public Double y;

        public Point() {}

        @Override
        public String toString() {
            return name;
        }
    }
}
