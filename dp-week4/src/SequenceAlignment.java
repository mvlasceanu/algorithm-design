import com.sun.xml.internal.fastinfoset.util.CharArray;

import java.io.*;
import java.util.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by rembrandt on 9/14/2015.
 */
public class SequenceAlignment {
    public Map<Character, Map<Character, Integer>> blossum;
    public ArrayList<Sequence> sequences = new ArrayList<>();
    public String fileName = "";

    public static void main(String[] args) {
        SequenceAlignment sa = new SequenceAlignment();
        sa.parseBlossum();


        if(args == null ||  args.length == 0)
        {
            throw new RuntimeException("Please specify the input file path");
        }
        for(String fl : args) {

            if (fl.trim().length() <= 4) {
                throw new RuntimeException("Invalid file name");
            }
            sa.fileName = fl;
            File inputFile = new File(fl);
            try {
                sa.parseInput(inputFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        for(int i = 0; i < sa.sequences.size(); i++) {
            for(int j = i+1; j < sa.sequences.size(); j++) {
                Sequence a = sa.sequences.get(i);
                Sequence b = sa.sequences.get(j);
                Alignment result = sa.needlemanWunch(a.sequence, b.sequence);
                String resultName = String.format("%s--%s", a.name, b.name);
                String altResultName = String.format("%s--%s", b.name, a.name);
                System.out.println(resultName + ": " + result.toString());
                try {
                    String r = sa.getValueInOut(resultName);
                    if (r == null) {
                        r = sa.getValueInOut(altResultName);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void parseBlossum()
    {
        // Blossum file name
        String BLOSSUM = "data/BLOSUM62.txt";
        File file = new File(BLOSSUM);
        blossum = new HashMap<>();
        try {
            FileInputStream fis = new FileInputStream(file);
            BufferedReader bsr = new BufferedReader(new InputStreamReader(fis));

            String line;
            String[] letters = null;
            while( (line = bsr.readLine()) != null) {

                // Ignore comments
                if (line.startsWith("#"))
                    continue;

                // Match first line containing all the letters
                if(line.startsWith(" "))
                {
                    String ln = line.trim();
                    letters = ln.split("\\s+");

                    // Go to the next line
                    continue;
                }

                if(letters != null && letters.length > 0) {
                    String[] valueLine = line.trim().split("\\s+");
                    Character mapKey = valueLine[0].charAt(0);
                    Map<Character, Integer> tempMap = new HashMap<>();

                    for (int i = 1; i < valueLine.length; i++) {
                        tempMap.put(letters[i - 1].charAt(0), Integer.parseInt(valueLine[i]));
                    }
                    blossum.put(mapKey, tempMap);
                }
            }

            //System.out.println("Finished parsing blossum");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void parseInput(File file) throws IOException {
        FileInputStream fis = new FileInputStream(file);
        BufferedReader bsr = new BufferedReader(new InputStreamReader(fis));

        String line;
        StringBuilder sb = new StringBuilder();

        while( (line = bsr.readLine()) != null) {
            sb.append(line);
            sb.append("\n");
        }

        Pattern p = Pattern.compile("^>([\\w-]*).*\\s+([\\w+\\s+]+)", Pattern.MULTILINE);

        Matcher matcher = p.matcher(sb.toString());

        while(matcher.find()) {
            String name = matcher.group(1);
            String sequence = matcher.group(2);

            Sequence seq = new Sequence(name, sequence.replace("\n","").replace("\r", ""));
            this.sequences.add(seq);
        }
        //System.out.println("Finished parsing the input file.");
    }

    public Alignment needlemanWunch(String a, String b){
        int[][] mScore = blossumToScoring(a,b);
        Cell[][] backtrackCells = initBackTrack(a, b);
        Cell tempCell;
        for(int i = 1; i < mScore.length; i++){
            for(int j=1; j < mScore[0].length; j++){

                char x = a.charAt(i-1);
                char y = b.charAt(j-1);

                int diag = mScore[i-1][j-1] + weight(x, y);
                int up = mScore[i-1][j] - 4;
                int left = mScore[i][j-1] - 4;

                int maxValue = diag;
                String direction = "D";
                tempCell = new Cell(x, y, backtrackCells[i-1][j-1]);

                if(maxValue < up){
                    maxValue = up;
                    tempCell = new Cell(x, '-', backtrackCells[i-1][j]);
                }
                if(maxValue < left){
                    maxValue = left;
                    tempCell = new Cell('-', y, backtrackCells[i][j-1]);
                }

                backtrackCells[i][j] = tempCell;
                mScore[i][j] = maxValue;

            }
        }
        int score = mScore[a.length()][b.length()];
        int maxLength = Math.max(a.length(), b.length());
        String sequenceA = "";
        String sequenceB = "";
        tempCell = backtrackCells[a.length()][b.length()];
        while(!tempCell.isDefault()){
            sequenceA += tempCell.aChar;
            sequenceB += tempCell.bChar;
            tempCell = tempCell.previousCell;
        }
        return new Alignment(score, new StringBuilder(sequenceA).reverse().toString(), new StringBuilder(sequenceB).reverse().toString());
    }

    public Cell[][] initBackTrack(String a, String b){
        Cell[][] backtrackMatrix = new Cell[a.length() + 1][b.length() + 1];

        backtrackMatrix[0][0] = new Cell();
        for(int j = 1; j < backtrackMatrix[0].length; j++){
            backtrackMatrix[0][j] = new Cell('-', b.charAt(j - 1), backtrackMatrix[0][j-1]);
        }
        for(int i = 1; i < backtrackMatrix.length; i++){

            backtrackMatrix[i][0] = new Cell(a.charAt(i - 1), '-', backtrackMatrix[i-1][0]);
        }
        return backtrackMatrix;
    }
    public int[][] blossumToScoring(String a, String b){
        int[][] scoreMatrix = new int[a.length() + 1][b.length() + 1];

        for(int j = 0; j < scoreMatrix[0].length; j++){
            scoreMatrix[0][j] = -4 * j;
        }
        for(int i = 0; i < scoreMatrix.length; i++){
            scoreMatrix[i][0] = -4 * i;
        }
        return scoreMatrix;
    }

    public String getValueInOut(String line)throws IOException {
        FileInputStream fis = new FileInputStream(this.fileName.replace("in.txt", "out.txt"));
        BufferedReader bsr = new BufferedReader(new InputStreamReader(fis));
        String ln;
        while( (ln = bsr.readLine()) != null) {
            if(ln.startsWith(line)) {
                return ln;
            }
        }

        return null;
    }

    private int weight(char a, char b){
        return blossum.get(a).get(b);
    }

    public class Cell{
        public char aChar;
        public char bChar;
        public Cell previousCell;
        public Cell(){
            this.aChar = '-';
            this.bChar = '-';
        }
        public Cell(char a, char b, Cell previousCell){
            this.aChar = a;
            this.bChar = b;
            this.previousCell = previousCell;
        }

        public boolean isDefault(){
            return previousCell == null;
        }
    }
    public class Sequence {
        public String name;
        public String sequence;

        public Sequence(String name, String sequence)
        {
            this.name = name;
            this.sequence = sequence;
        }
    }

    public class Alignment{
        public int score;
        public String sequenceA;
        public String sequenceB;

        public Alignment(int score, String sequenceA, String sequenceB){
            this.score = score;
            this.sequenceA = sequenceA;
            this.sequenceB = sequenceB;
        }

        @Override
        public String toString() {
            return this.score + "\n" + this.sequenceA + "\n" + this.sequenceB;
        }
    }
}
