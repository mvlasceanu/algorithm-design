import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by edgars on 9/21/15.
 */
public class Flow {
    public List<Node> nodes;
    public List<Node> minCut;
    public List<Edge> edges;
    public Map<Edge, Integer> flow = new HashMap<>();

    public static void main(String[] args) {
        String file = args[0];
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fileInputStream));
            Flow f = new Flow();
            List<Node> nodes = f.parse(br);
            f.nodes = nodes;

            Node source = nodes.get(0);
            Node sink = nodes.get(nodes.size() - 1);

            int minFlow = f.fordFulkerson(source, sink);
            System.out.println("MinFlow: " + minFlow);
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public List<Edge> findPath(Node source, Node sink) {
        minCut = new ArrayList<>();
        Queue<Node> queue = new LinkedList<>();
        queue.add(source);
        List<Node> path = new ArrayList<>();
        boolean isPath = false;
        while (!queue.isEmpty()) {
            Node n = queue.remove();

            for (Edge e : n.edges) {
                Node snk = e.sink;

                if (e.residualFlow() > 0 && !path.contains(snk)) {
                    snk.incomingEdge = e;
                    minCut.add(snk);
                    path.add(snk);
                    if (snk.equals(sink)) {
                        isPath = true;
                        break;
                    }
                    queue.add(snk);
                }
            }
        }

        List<Edge> augmentationPath = new ArrayList<>();

        if (isPath) {
            Node current = sink;

            while (!current.equals(source)) {
                augmentationPath.add(current.incomingEdge);
                current = current.incomingEdge.source;
            }
        }
        Collections.reverse(augmentationPath);
        return augmentationPath;
    }

    /**
     * @param source
     * @param sink
     * @return
     */
    public int fordFulkerson(Node source, Node sink) {

        List<Edge> path = findPath(source, sink);
        List<Integer> residuals = null;
        int maxFlow = 0;
        while (path != null && path.size() > 0) {
            int bottleneck = Integer.MAX_VALUE;
            residuals = path.stream().map(e -> e.residualFlow()).collect(Collectors.toList());
            bottleneck = Math.min(bottleneck, Collections.min(residuals));
            for (Edge e : path) {
                e.currentFlow += bottleneck;
                e.reverseEdge.currentFlow -= bottleneck;
            }
            maxFlow += bottleneck;

            path = findPath(source, sink);
        }
        edges.stream().filter(e -> minCut.contains(e.source) && !minCut.contains(e.sink)).forEach(System.out::println);

        return maxFlow;
    }

    public List<Node> parse(BufferedReader br) {
        try {
            edges = new ArrayList<>();
            int nNodes = Integer.parseInt(br.readLine());
            List<Node> nodes = new ArrayList<>();
            for (int i = 0; i < nNodes; i++) {
                String line = br.readLine();
                nodes.add(new Node(line, i));
            }

            int mEdges = Integer.parseInt(br.readLine());

            for (int i = 0; i < mEdges; i++) {
                String line = br.readLine();
                String[] params = line.split("\\s+");

                Node source = nodes.get(Integer.parseInt(params[0]));
                Node sink = nodes.get(Integer.parseInt(params[1]));
                int capacity = Integer.parseInt(params[2]);

                if (capacity == -1) {
                    capacity = Integer.MAX_VALUE;
                }
                Edge edge = new Edge(source, sink, capacity);
                Edge reverseEdge = new Edge(sink, source, capacity);
                edge.reverseEdge = reverseEdge;
                reverseEdge.reverseEdge = edge;
                edges.add(edge);
                source.edges.add(edge);
                sink.edges.add(reverseEdge);


                flow.put(edge, 0);
                flow.put(reverseEdge, 0);
            }
            return nodes;
        } catch (IOException ex) {
            System.out.print(ex.getMessage());
        }
        return null;
    }

    public class Node {
        public String name;
        public int index;
        public List<Edge> edges;
        public Edge incomingEdge;

        public Node(String name, int index) {
            edges = new ArrayList<>();
            this.index = index;
            this.name = name;
        }

    }

    public class Edge {
        public Node source;
        public Node sink;
        public Edge reverseEdge;
        public int capacity;
        public int currentFlow = 0;

        public Edge(Node source, Node out, int capacity) {
            this.source = source;
            this.sink = out;
            this.capacity = capacity;
        }

        public int residualFlow() {
            return capacity - currentFlow;
        }

        @Override
        public String toString() {
            return String.format("%d %d %d", source.index, sink.index, capacity);
        }
    }
}

