import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Created by edgars on 8/31/15.
 */
public class IP {


    public static void main(String[] params) throws Exception{
        FileInputStream fstream = new FileInputStream(params[0]);
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        IP ip = new IP();
        List<Job> jobs = ip.parse(br);
        List<Job> sortedJobs = new ArrayList<Job>();
        for(Job j : jobs){
            sortedJobs.add(j);
        }

        Collections.sort(sortedJobs, new Comparator<Job>() {
            @Override
            public int compare(Job o1, Job o2) {
                return o1.startTime.compareTo(o2.startTime);
            }
        });
        System.out.println("SORTED!");
        ip.assignRooms(sortedJobs);

        for(Job j : jobs){
            System.out.println(j.toString());
        }

    }

    private void assignRooms(List<Job> jobs){
        PriorityQueue<Room> rooms = new PriorityQueue<Room>(new Comparator<Room>() {
            @Override
            public int compare(Room o1, Room o2) {
                return o1.currentEndTime.compareTo(o2.currentEndTime);
            }
        });
        rooms.add(new Room(0));
        int roomCount = 1;

        for(Job job : jobs){
            Room r  = rooms.poll();
            if(!r.conflict(job)){
                r.addJob(job);
                job.room = r.number;
            }
            else{
                Room room = new Room(roomCount);
                room.addJob(job);
                rooms.add(room);
                job.room = roomCount;
                roomCount++;
            }
            rooms.add(r);
        }

        System.out.println(rooms.size() + "\n");

    }

    private List<Job> parse(BufferedReader br) throws Exception{
        List<Job> jobs = new ArrayList<Job>();
        int n = Integer.parseInt(br.readLine());
        br.readLine();
        for(int i = 0; i < n; i++){
            String[] line = br.readLine().split(" ");
            jobs.add(new Job(Integer.parseInt(line[0]), Integer.parseInt(line[1])));
        }

        return jobs;
    }


    private class Room{
        public int number;
        public List<Job> jobs;
        public Integer currentEndTime;

        public Room(int number){
            this.number = number;
            this.jobs = new ArrayList<Job>();
            currentEndTime = 0;
        }

        public boolean conflict(Job job){
            return job.startTime < currentEndTime;
        }
        public void addJob(Job job){
            this.currentEndTime = job.endTime;
            this.jobs.add(job);
        }
    }
    private class Job{
        public Integer startTime;
        public Integer endTime;
        public Integer room;
        public Job(int startTime, int endTime){
            this.startTime = startTime;
            this.endTime = endTime;
            this.room = -1;
        }

        @Override
        public String toString(){
            return startTime + " " + endTime + " " + room;
        }
    }
}
