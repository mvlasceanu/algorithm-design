package dk.itu.ad1.smweek1.groupm;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
public class Main {
    public Person[] persons;
    public String filenames;
    public int input = 0;
    public ArrayList<Person> unmatched = new ArrayList<>();
    public Main(String[] params) {
        String filename = params[0];
        String file = filename;
        if(!filename.contains(".txt")) { file = filename + ".txt"; }
        if (!filename.contains("out") && !filename.contains("log")) {
            this.filenames = file;
            this.persons = null;
            this.input = 0;
            this.unmatched = new ArrayList<Person>();
            this.setup();
            this.match();
        } else { System.out.println("Don't input the output file!"); }
    }
    public void setup() {
        try {
            FileInputStream fstream = new FileInputStream(this.filenames);
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                String line = strLine;
                if(strLine.startsWith("\\#")) continue;
                if(line.startsWith("n="))  {
                    this.input = Integer.parseInt(line.replace("n=", ""));
                    persons = new Person[input*2];
                }
                if(line.matches("([0-9]+)[\\ ](.*)")) {
                    String[] parts = line.split(" ");
                    Person person = new Person();
                    person.name =  parts[1];
                    person.number = Integer.parseInt(parts[0]);
                    int index = Integer.parseInt(parts[0]) - 1;
                    persons[index] = person;
                }
                if(line.matches("([0-9]+)[\\:][\\s][\\d,\\s]+")) {
                    int columnIndex = line.indexOf(":");
                    Person person = getPersonById(Integer.parseInt(line.substring(0, columnIndex)));
                    String ln = line.substring(columnIndex + 1, line.length());
                    String[] numbers = ln.split("\\s");
                    for (String n : numbers) {
                        if(n != null && n.trim().length() > 0)
                            person.preferences.add( getPersonById(Integer.parseInt(n)) );
                    }
                }
            }
            for(Person p : this.persons) {
                this.unmatched.add(p);
            }
            br.close();
        } catch (FileNotFoundException e) {
        } catch (IOException e) { }
    }
    public Person getFirstOdd() {
        for(Person p : this.unmatched)  {  if(p.number % 2 > 0) { return p; } }
        return null;
    }
    public void match() {
        while(!this.unmatched.isEmpty()) {
            Person p = getFirstOdd();
            for(Person person : p.preferences) {
                if(person != null) {
                    boolean accepted = propose(p, person);
                    if (accepted) break;
                }
            }
        }
        for(Person p : this.persons) {
            if(p.number % 2 > 0) {
                System.out.println((String.format("%s -- %s", p.name, p.matched_to.name)));
            }
        }
    }
    public boolean propose(Person proposing, Person proposed) {
        if(!proposed.isMatched()) {
            proposing.matched_to = proposed;
            proposed.matched_to = proposing;
            proposing.preferences.remove(proposed);
            this.unmatched.remove(proposing);
            this.unmatched.remove(proposed);
            return true;
        } else {
            if(proposed.prefers(proposing)) {
                Person kickedOut = proposed.matched_to;
                proposing.matched_to = proposed;
                proposed.matched_to = proposing;
                proposing.preferences.remove(proposed);
                this.unmatched.remove(proposing);
                this.unmatched.remove(proposed);
                kickedOut.matched_to = null;
                this.unmatched.add(kickedOut);
                return true;
            }
        }
        return false;
    }
    public Person getPersonById(int id)
    {
        for(Person p : this.persons) {
            if(p == null) continue;
            if(p instanceof Person && p.number == id)  return p;
        }
        return null;
    }
    public static void main(String[] args) {
        Main main = new Main(args);
    }
    public class Person {
        public String name;
        public int number;
        public Person matched_to;
        public List<Person> preferences = new ArrayList<>();
        public Person() {}
        public boolean isMatched() { return this.matched_to != null;  }
        public boolean prefers(Person person) {
            if(!this.isMatched()) return true;
            return (this.preferences.indexOf(person) < this.preferences.indexOf(this.matched_to));
        }
    }
}